<?php

$name = $_GET['name'];
$filename = __DIR__ . '/uploads/' . $name;

if (file_exists($filename)) {
    unlink($filename);
}

header('Location: /');
