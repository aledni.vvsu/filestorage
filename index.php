<?php

    $uploadDir = __DIR__ . '/uploads/';

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $image = $_FILES['image'];
        $destination = $uploadDir . $image['name'];

        $isSucceed = move_uploaded_file($image['tmp_name'], $destination);

        if ($isSucceed) {
            header('Location: /');
        } else {
            echo 'Не удалось загрузить файл';
        }
    }

    $files = [];

    $fileList = array_filter(scandir($uploadDir), function ($file) {
        return strpos($file, '.') !== 0;
    });

    foreach ($fileList as $file) {
        $files[] = [
            'name' => $file,
            'size' => filesize($uploadDir . $file)
        ];
    }

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Файловое хранилище</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
</head>
<body>
    <div class="container bg-light py-5">
        <h1>Мои файлы</h1>

        <div class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis dolor eaque error necessitatibus, nihil non praesentium quam sit tenetur voluptatem.</div>

        <div class="row">
            <div class="col-md-3">
                <h4>Загрузка файла</h4>

                <form method="post" enctype="multipart/form-data">
                    <input class="form-control" type="file" name="image" />

                    <button class="mt-2 btn btn-success">Загрузить</button>
                </form>
            </div>
            <div class="col-md-9">
                <h4>Список файлы</h4>

                <table class="table">
                    <thead>
                    <tr>
                        <th>Имя файла</th>
                        <th>Размер</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($files as $file): ?>
                            <tr>
                                <td><?=$file['name']?></td>
                                <td><?=round($file['size'] / 1024, 2) . ' KB'?></td>
                                <td><a class="btn btn-danger" href="delete.php?name=<?=$file['name']?>"><i class="bi bi-trash"></i></a></td>
                                <td><a class="btn btn-dark" href="download.php?name=<?=$file['name']?>"><i class="bi bi-cloud-download"></i>
                                    </a></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>